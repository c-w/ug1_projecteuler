import Char
import Data.List
import Data.List.Split
import Data.Numbers
import Data.Numbers.Primes
import Data.Ratio
import List
import Monad



-- Problem 1 (Haskell)

pb1 = foldr (+) 0 ( filter (\x -> x `mod` 3 == 0 || x `mod` 5 == 0) [1..999] )



-- Problem 2 (Haskell)

pb2 = foldr (+) 0 ( ( filter even ( takeWhile (<4000000) (drop 2 fibs) ) ) )
	where
	fibs = 0 : 1 : zipWith (+) fibs (tail fibs)



-- Problem 3 (Haskell)

pb3 = last ( primeFactors 600851475143 )



-- Problem 4 (Haskell)

pb4 = maximum [ a*b | a <- [1..999], b <- [1..999], isPalin (a*b) ]
	where
	isPalin x = show x == reverse (show x)



-- Problem 5 (Haskell)

pb5 = foldr lcm 1 [1..20]



-- Problem 6 (Haskell)

pb6 = ( foldr (+) 0 [1..100] )^2 - foldr (+) 0 ( map (^2) [1..100] )



-- Problem 7 (Haskell)

pb7 = primes !! 10001



-- Problem 8 (Haskell)

pb8 = maximum ( consecs ( map digitToInt ( show k ) ) )
	where
	consecs (a:b:c:d:e:xs) = a*b*c*d*e : consecs (b:c:d:e:xs)
	consecs _ = []
	k = 7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450



-- Problem 9 (Haskell)

pb9 = product ( head [ [a,b,c] | m <- [1..1000], n <- [1..m], let a = m^2-n^2, let b = 2*m*n, let c = m^2+n^2, a+b+c == 1000 ] )



-- Problem 10 (Haskell)

pb10 = foldr (+) 0 ( takeWhile (<2000000) primes )



-- Problem 11 (Haskell)

pb11 = do
	src <- readFile "pb011.txt"
	let f1 = splitOn "\n" src
	let f2 = map (splitOn " ") f1
	let f3 = map (map toInt) f2
	let maxLR = maximum $ map (maximum2 . prodH) f3
	let maxUD = maximum $ map (maximum2 . prodH) (transpose f3)
	let maxDR = maximum $ map (maximum2 . prodH) (rows1 f3)
	let maxDL = maximum $ map (maximum2 . prodH) (rows2 f3)
	print $ maxLR `max` maxUD `max` maxDR `max` maxDL
		where
		toInt x = read x :: Integer
		prodH (a:b:c:d:rest) = a*b*c*d : prodH (b:c:d:rest)
		prodH _ = []

		getIt :: [[Integer]] -> Int -> Int -> Integer
		getIt xss line num = (xss !! line) !! num

		row1 :: [[Integer]] -> Int -> Int -> [Integer]
		row1 _ _ 19 = []
		row1 _ 19 _ = []
		row1 [] _ _ = []
		row1 xss i j = getIt xss i j : row1 xss (i+1) (j+1)

		row2 :: [[Integer]] -> Int -> Int -> [Integer]
		row2 _ _ 0 = []
		row2 _ 19 _ = []
		row2 [] _ _ = []
		row2 xss i j = getIt xss i j : row2 xss (i+1) (j-1)

		rows1 xss = [ row1 xss a b | a <- [0..19], b <- [0..19] ]

		rows2 xss = [ row2 xss a b | a <- [0..19], b <- [0..19] ]

		maximum2 xs = if xs == [] then 0 else maximum xs



-- Problem 12 (Haskell)

pb12 = head ( filter ( (>500) . numOfFactors) triangles )
	where
	triangles = scanl1 (+) [1..]



-- Problem 13 (Haskell)

pb13 = do
	list <- getLines "pb013.txt"
	let list2 = map toInt list
	let x = foldr (+) 0 list2
	let k = toInt ( take 10 (show x) )
	print k
		where
		getLines = liftM lines . readFile
		toInt x = read x :: Integer



-- Problem 14 (Haskell)

pb14 = snd $ maximum $ map (\x -> (lchain x, x)) [1..1000000]
	where
	lchain = length . chain
	chain n = (takeWhile (>1) $ iterate step n) ++ [1]
	step n = if even n then n `div` 2 else 3*n+1



-- Problem 15 (Haskell)

pb15 = choose (2*20) 20
	where
	fact n = product [1..n]
	choose n r = fact n `div` (fact r * fact (n-r))



-- Problem 16 (Haskell)

pb16 = sumDigs (2^1000)
	where
	sumDigs :: Integer -> Int
	sumDigs x = foldr (+) 0 (getDigs x)

	getDigs :: Integer -> [Int]
	getDigs x = map digitToInt (show x)



-- Problem 17 (Haskell)

pb17 = foldr (+) 0 (map len [1..1000])
	where
	len n = length (filter (\x -> x /= ' ' && x /= '-') (intToWord n))

	intToWord 0 = ""
	intToWord 1 = "one"
	intToWord 2 = "two"
	intToWord 3 = "three"
	intToWord 4 = "four" 
	intToWord 5 = "five"
	intToWord 6 = "six"
	intToWord 7 = "seven"
	intToWord 8 = "eight"
	intToWord 9 = "nine"
	intToWord 10 = "ten"
	intToWord 11 = "eleven"
	intToWord 12 = "twelve"
	intToWord 13 = "thirteen"
	intToWord 14 = "fourteen"
	intToWord 15 = "fifteen"
	intToWord 16 = "sixteen"
	intToWord 17 = "seventeen"
	intToWord 18 = "eighteen"
	intToWord 19 = "nineteen"
	intToWord 20 = "twenty"
	intToWord x | x > 20 && x < 30 = "twenty-" ++ intToWord (x-20)
	intToWord 30 = "thirty"
	intToWord x | x > 30 && x < 40 = "thirty-" ++ intToWord (x-30)
	intToWord 40 = "forty"
	intToWord x | x > 40 && x < 50 = "forty-" ++ intToWord (x-40)
	intToWord 50 = "fifty"
	intToWord x | x > 50 && x < 60 = "fifty-" ++ intToWord (x-50)
	intToWord 60 = "sixty"
	intToWord x | x > 60 && x < 70 = "sixty-" ++ intToWord (x-60)
	intToWord 70 = "seventy"
	intToWord x | x > 70 && x < 80 = "seventy-" ++ intToWord (x-70)
	intToWord 80 = "eighty"
	intToWord x | x > 80 && x < 90 = "eighty-" ++ intToWord (x-80)
	intToWord 90 = "ninety"
	intToWord x | x > 90 && x < 100 = "ninety-" ++ intToWord (x-90)
	intToWord x | x > 99 && x < 1000 = hundreds x ++ subs x
		where
		hundreds x = intToWord (div x 100) ++ " hundred"
		subs x | rem x 100 /= 0 = " and " ++ intToWord (rem x 100) | otherwise = ""
	intToWord 1000 = "one thousand"



-- Problem 19 (Haskell)

pb19 = length $ filter (\(y,m,nameDay,date) -> nameDay == "su" && date == 1) cals
	where
	cals = filter (\(year,m,n,d) -> year >= 1901) $ concat $ map cal [1900..2000]

	cal y = jan y ++ feb y ++ mar y ++ apr y ++ mai y ++ jun y ++ jul y ++ aug y ++ sep y ++ oct y ++ nov y ++ dec y
		where
		jan y = [ (y,"jan",v,w) |	let a = nDays y !! 0,
					(v,w) <- zip (take a (cDays y)) [1..a] ]

		feb y = [ (y,"feb",v,w) |	let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take b (drop a $ cDays y)) [1..b] ]

		mar y = [ (y,"mar",v,w) |	let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take c (drop b $ drop a $ cDays y)) [1..c] ]

		apr y = [ (y,"apr",v,w) |	let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take d (drop c $ drop b $ drop a $ cDays y)) [1..d] ]

		mai y = [ (y,"mai",v,w) |	let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take e (drop d $ drop c $ drop b $ drop a $ cDays y)) [1..e] ]

		jun y = [ (y,"jun",v,w) |	let f = nDays y !! 5,
									let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take f (drop e $ drop d $ drop c $ drop b $ drop a $ cDays y)) [1..f] ]

		jul y = [ (y,"jul",v,w) |	let g = nDays y !! 6,
									let f = nDays y !! 5,
									let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take g (drop f $ drop e $ drop d $ drop c $ drop b $ drop a $ cDays y)) [1..g] ]

		aug y = [ (y,"aug",v,w) |	let h = nDays y !! 7,
									let g = nDays y !! 6,
									let f = nDays y !! 5,
									let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take h (drop g $ drop f $ drop e $ drop d $ drop c $ drop b $ drop a $ cDays y)) [1..h] ]

		sep y = [ (y,"sep",v,w) |	let i = nDays y !! 8,
									let h = nDays y !! 7,
									let g = nDays y !! 6,
									let f = nDays y !! 5,
									let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take i (drop h $ drop g $ drop f $ drop e $ drop d $ drop c $ drop b $ drop a $ cDays y)) [1..i] ]

		oct y = [ (y,"oct",v,w) |	let j = nDays y !! 9,
									let i = nDays y !! 8,
									let h = nDays y !! 7,
									let g = nDays y !! 6,
									let f = nDays y !! 5,
									let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take j (drop i $ drop h $ drop g $ drop f $ drop e $ drop d $ drop c $ drop b $ drop a $ cDays y)) [1..j] ]

		nov y = [ (y,"nov",v,w) |	let k = nDays y !! 10,
									let j = nDays y !! 9,
									let i = nDays y !! 8,
									let h = nDays y !! 7,
									let g = nDays y !! 6,
									let f = nDays y !! 5,
									let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take k (drop j $ drop i $ drop h $ drop g $ drop f $ drop e $ drop d $ drop c $ drop b $ drop a $ cDays y)) [1..k] ]

		dec y = [ (y,"dec",v,w) |	let l = nDays y !! 11,
									let k = nDays y !! 10,
									let j = nDays y !! 9,
									let i = nDays y !! 8,
									let h = nDays y !! 7,
									let g = nDays y !! 6,
									let f = nDays y !! 5,
									let e = nDays y !! 4,
									let d = nDays y !! 3,
									let c = nDays y !! 2,
									let b = nDays y !! 1,
									let a = nDays y !! 0,
					(v,w) <- zip (take l (drop k $ drop j $ drop i $ drop h $ drop g $ drop f $ drop e $ drop d $ drop c $ drop b $ drop a $ cDays y)) [1..l] ]

	leapYear x
		| x `mod` 100 == 0 && x `mod` 400 == 0
		= True
		| x `mod` 100 /= 0 && x `mod` 4 == 0
		= True
		| otherwise
		= False

	cDays y
		| y > 1900
		= take (numOfDays y) $ drop (sumDays y - if leapYear y then 2 else 1) allDays
		| y == 1900
		= take (numOfDays y) allDays

	allDays = cycle ["mo","tu","we","th","fr","sa","su"]

	nDays x = [31,if leapYear x then 29 else 28,31,30,31,30,31,31,30,31,30,31]

	numOfDays = sum . nDays

	sumDays x = sum $ map numOfDays [1900..x]



-- Problem 20 (Haskell)

pb20 = sumDigs (fact 100)
	where
	sumDigs :: Integer -> Int
	sumDigs x = foldr (+) 0 (getDigs x)

	getDigs :: Integer -> [Int]
	getDigs x = map digitToInt (show x)

	fact n = product [1..n]



-- Problem 21 (Haskell)

pb21 = sum [ a | a <- [1..10000], let b = d a, d b == a, a /= b ]
	where
	d = sum . divisors
	divisors x = [ a | a <- [1..x `div` 2], rem x a == 0 ]



-- Problem 22 (Haskell)

pb22 = do
	input <- readFile "pb022.txt"
	let f = filter (/= '\"') input
	let list = splitOn "," f
	let sorted = sort list
	let toValue = map alphValue sorted
	let index = zip [1..] toValue
	let score = map (\(a,b) -> a*b) index
	let final = foldr (+) 0 score
	print final
		where
		alphValue = foldr (+) 0 . map (\x -> ord x - ord 'A' + 1)



-- Problem 23 (Haskell)

pb23 = sum $ filter (not . isAbundantSum) curious
	where
	curious = [1..48] ++ ( filter odd [49..20161] )
	
	isAbundantSum x = ( take 1 $ [ (m,n) | m <- absTo x, n <- absTo x,  x == m + n ] ) /= []
		where
		absTo z = takeWhile (<= z) abundants
		abundants = filter abundant [1..20161]
		abundant n = sum ( factors n ) > n



-- Problem 24 (Haskell)

pb24 = toNum ( filter (\x -> not ( x `elem` "[]," )) (show getIt) )
	where
	getIt = ( sort ( permutations [0,1,2,3,4,5,6,7,8,9] ) ) !! (1000000-1)
	toNum x = read x :: Integer



-- Problem 25 (Haskell)

pb25 = length (takeWhile (<10^999) fibs)
	where
	fibs = 0 : 1 : zipWith (+) fibs (tail fibs)



-- Problem 26 (Haskell)

pb26 = snd $ maximum $ map (\n -> (period n, n)) cands -- http://goo.gl/0OH1C
	where
	cands = dropWhile (<=5) $ takeWhile (<1000) primes
	
	period p = (order 10 p) `mod` p
	order x p = head [ k | k <- [1..], x^k `mod` p == 1 ]



-- Problem 27 (Haskell)

pb27 = (\(_,(_,a,b)) -> a*b) $ maximum $ map (\x -> (getConsecPrimes x 0,x)) mkEqs
	where
	mkEqs = [ (1,a,b) | --fx = n^2 + a*n + b
				a <- [-1000..1000],
				b <- filter isPrime [-1000..1000], --f0 prime
				isPrime (a+b+1) ] --f1 prime

	getConsecPrimes (1,a,b) n
		| isPrime $ abs (n*n + a*n + b)
		= 1 + getConsecPrimes (1,a,b) (n+1)
		| otherwise
		= 0



-- Problem 28 (Haskell)

pb28 = sum $ takeWhile (<=1001*1001) spirs
	where
	spir 0 = take 5 ( iterate (+2) 1 )
	spir n = take 5 ( iterate (\x -> x + (2*(n+1))) $ last (spir (n-1)) )

	spirs = nub $ foldr (++) [] $ map spir [0..]



-- Problem 29 (Haskell)

pb29 = len 100
	where
	len = length . sort . nub . list
	list x = [ a^b | a <- [2..x], b <- [2..x] ]



-- Problem 30 (Haskell)

pb30 = sum [ x |	x <- [2..1000000],
					sumPowers x == fromIntegral x ]
	where
	sumPowers = foldr (+) 0 . map (^5) . getDigs

	getDigs :: Integer -> [Int]
	getDigs x = sort (map digitToInt (show x))



-- Problem 31 (Haskell)

xs = [ (a,b,c,d,e,f,g,h) |
		a <- [0..200],
		b <- [0..100],
		c <- [0..40],
		d <- [0..20],
		e <- [0..10],
		f <- [0..4],
		g <- [0..2],
		h <- [0..1],
		a+b+c+d+e+f+g+h == 200 ]



-- Problem 33 (Haskell)

pb33 = denominator ( getFraction ( multPair ( unzip ( map unShow theOnes ) ) ) )
	where
	getFraction (a,b) = (a % 1) / (b % 1)
	
	multPair (xs,ys) = (foldr (*) 1 xs, foldr (*) 1 ys)

	unShow :: (String,String) -> (Int,Int)
	unShow (xs,ys) = (read xs, read ys)

	theOnes = filter cancelable ( map show' candidates )
		where
		cancelable :: (String,String) -> Bool
		cancelable ((a:b:[]),(c:d:[]))
			| a == c
			= b'/d' == ab/cd
			| a == d
			= b'/c' == ab/cd
			| b == c
			= a'/d' == ab/cd
			| b == d
			= a'/c' == ab/cd
				where
				a' = fromIntegral (digitToInt a)
				b' = fromIntegral (digitToInt b)
				c' = fromIntegral (digitToInt c)
				d' = fromIntegral (digitToInt d)
				ab = toInt (a:b:[])
				cd = toInt (c:d:[])
				toInt x = read x :: Float
		
		show' (a,b) = (show a, show b)
		
		candidates = filter sames allFracts
			where
			sames (a,b) = any (\x -> x `elem` a') b'
				where
				a' = show a
				b' = show b
			allFracts = filter
					(\(a,b) -> mod a 10 /= 0 || mod b 10 /= 0)
					[ (a,b) |
						a <- [10..99],
						b <- [10..99],
						div a b == 0 ]



-- Problem 34 (Haskell)

pb34 = sum [ x |	x <- [3..100000],
					sumFacts x == fromIntegral x ]
	where
	sumFacts = foldr (+) 0 . map fact . getDigs

	getDigs :: Integer -> [Int]
	getDigs x = sort (map digitToInt (show x))

	fact x = product [1..x]



-- Problem 35 (Haskell)

pb35 = length [ p | p <- primes', all isPrime (rotate p) ]
	where
	primes' = takeWhile (<1000000) primes

	rotate :: Integer -> [Integer]
	rotate = map toInt . rotations . show
		where
		toInt x = read x :: Integer
		rotations xs = take (length xs) (rot xs)
		rot (x:xs) = (x:xs) : rot (xs++[x])



-- Problem 36 (Haskell)

pb36 = sum ( filter (\x -> isPalin x && isPalin (binary x)) [1..1000000] )
	where
	binary n = read ( reverse (toBinary n) ) :: Integer
		where
		toBinary 0 = "0"
		toBinary 1 = "1"
		toBinary n = show (n `rem` 2) ++ toBinary (n `div` 2)

	isPalin x = show x == reverse (show x)



-- Problem 37 (Haskell)

pb37 = sum $ take 11 $ dropWhile (<=7) tPrimes
	where
	tPrimes = [ p | p <- primes, all isPrime' $ truncs p ]

	isPrime' x = if x == 1 then False else isPrime x

	toInt x = read x :: Integer	
	
	truncs x = map toInt ( truncsL (show x) ++ truncsR (show x) )

	truncsL [] = []
	truncsL xs = xs : truncsL (drop 1 xs)

	truncsR [] = []
	truncsR xs = xs : truncsR (take (length xs-1) xs)



-- Problem 39 (Haskell)

pb39 = snd $ maximum $ map (\n -> (length $ perim n, n)) [1..1000]
	where
	perim p = filter (\(a,b,c) -> a+b+c == p) allTriples
	
	sortP (a,b,c)
		| a <= b && b <= c
		= (a,b,c)
		| a <= c && c <= b
		= (a,c,b)
		| b <= a && a <= c
		= (b,a,c)
		| b <= c && c <= a
		= (b,c,a)
		| c <= a && a <= b
		= (c,a,b)
		| c <= b && b <= a
		= (c,b,a)

	allTriples = nub $ map sortP [ (m*a,m*b,m*c) | (a,b,c) <- primTriples, m <- [1..1000 `div` c] ]

	primTriples = [ (a,b,c) | n <- [1..rt 1000], m <- [n+1..rt 1000], let a = m^2-n^2, let b = 2*m*n, let c = m^2+n^2 ]

	rt = ceiling . sqrt . fromInteger



-- Problem 40 (Haskell)

pb40 = d 1 * d 10 * d 100 * d 1000 * d 10000 * d 100000 * d 1000000
	where
	d n = digitToInt (num !! (n-1))
	num = concat [ show x | x <- [1..] ]



-- Problem 41 (Haskell)

pb41 = head $ filter (pandigitalTo 7) $ primesDigits 7
	where
	primesDigits n = filter isPrime [10^n-1,10^n-2..10^(n-1)]
	
	pandigitalTo n x = sort ( show x ) == concat ( map show [1..n] )



-- Problem 42 (Haskell)

pb42 = do
	src <- readFile "pb042.txt"
	let f = filter (/= '\"') src
	let split = splitOn "," f
	let toValue = map alphValue split
	let trgs = filter isTriangle toValue
	print (length trgs)
		where
		alphValue = foldr (+) 0 . map (\x -> ord x - ord 'A' + 1)
		isTriangle x = isSquare (8*x+1)
		isSquare a = [ k | k <- [1..(rt a)], k*k == a ] /= []
		rt = ceiling . sqrt . fromIntegral



-- Problem 43 (Haskell)

pb43 = sum p7s
	where
	p7s = filter p7 p6s
	p6s = filter p6 p5s
	p5s = filter p5 p4s
	p4s = filter p4 p3s
	p3s = filter p3 p2s
	p2s = filter p2 p1s
	p1s = filter p1 pandigitals

	pandigitals = map toInt ( permutations "0123456789" )

	p1 n = toInt (d 2 n ++ d 3 n ++ d 4 n) `rem` 2 == 0
	p2 n = toInt (d 3 n ++ d 4 n ++ d 5 n) `rem` 3 == 0
	p3 n = toInt (d 4 n ++ d 5 n ++ d 6 n) `rem` 5 == 0
	p4 n = toInt (d 5 n ++ d 6 n ++ d 7 n) `rem` 7 == 0
	p5 n = toInt (d 6 n ++ d 7 n ++ d 8 n) `rem` 11 == 0
	p6 n = toInt (d 7 n ++ d 8 n ++ d 9 n) `rem` 13 == 0
	p7 n = toInt (d 8 n ++ d 9 n ++ d 10 n) `rem` 17 == 0
	
	d x = drop (x-1) . take x . show

	toInt x = read x :: Integer



-- Problem 44 (Haskell)

pb44 = (\(a,b) -> if b > a then b-a else a-b) getIt
	where
	getIt = head [ (j,k) | j <- pentagonals, k <- pentagonals, k > j, isPentagonal (j+k), isPentagonal (k-j) ]

	pentagonals :: [Integer]
	pentagonals = map (\n -> n*(3*n-1) `div` 2) [1..10000]

	isPentagonal :: Integer -> Bool
	isPentagonal x = isInt n
		where
		n = (rt (24*x+1) + 1) / 6
		rt = sqrt . fromInteger
		isInt x = x == fromInteger (round x)



-- Problem 45 (Haskell)

pb45 = head ( dropWhile (<= 40755) [ x | x <- triangles, isPentagonal x, isHexagonal x ] )
	where
	triangles = map (\n -> n*(n+1) `div` 2) [1..]

	isPentagonal :: Integer -> Bool
	isPentagonal x = isInt n
		where
		n = (rt (24*x+1) + 1) / 6
		rt = sqrt . fromInteger
		isInt x = x == fromInteger (round x)
		
	isHexagonal :: Integer -> Bool
	isHexagonal x = isInt n
		where
		n = (rt (8*x+1) + 1) / 4
		rt = sqrt . fromInteger
		isInt x = x == fromInteger (round x)



-- Problem 46 (Haskell)

pb46 = head ( filter (not . test) compositeOdds )
	where
	test n = any isPrime ( takeWhile (>0) ( ( map (\i -> n - 2*i*i ) ) [1..] ) )

	compositeOdds = filter (not . isPrime) [3,5..]



-- Problem 47 (Haskell)

pb47 = head d'
	where
	d' = filter (\(a,b,c,d) -> distinctPrimeFactors d == 4) c'
	c' = filter (\(a,b,c,d) -> distinctPrimeFactors c == 4) b'
	b' = filter (\(a,b,c,d) -> distinctPrimeFactors b == 4) a'
	a' = filter (\(a,b,c,d) -> distinctPrimeFactors a == 4) abcd
	abcd = [ (a,b,c,d) | a <- [1..200000], let b = a+1, let c = b+1, let d = c+1 ]

	distinctPrimeFactors = distincts . primeFactors

	distincts xs = sum ( zipWith (\x y -> if x /= y then 1 else 0) xs (tail xs) ) + 1



-- Problem 48 (Haskell)

pb48 = drop (length n-10) n
	where
	n = show (series 1000)
	series x = sum [ a^a | a <- [1..x] ]



-- Problem 49 (Haskell)

pb49 = read ((\(a,b,c) -> show a ++ show b ++ show c) (getIt !! 2)) :: Integer
	where
	getIt = [ (a,b,c) | a <- primes', b <- primes', c <- primes', c-b == b-a, c > b, b > a, allPerms a b c ]
	
	primes' = takeWhile (<9999) ( dropWhile (<999) ( primes ) )
	
	allPerms a b c = b' `elem` permutations a' && c' `elem` permutations a'
		where
		a' = show a
		b' = show b
		c' = show c



-- Problem 52 (Haskell)

pb52 = head [ x |
				x <- [1..],
				getDigs (2*x) == getDigs (3*x),
				getDigs (3*x) == getDigs (4*x),
				getDigs (4*x) == getDigs (5*x),
				getDigs (5*x) == getDigs (6*x) ]
	where
	getDigs :: Integer -> [Int]
	getDigs x = sort (map digitToInt (show x))



-- Problem 53 (Haskell)

pb53 = length (filter (>1000000) [ n `c` r | n <- [1..100], r <- [n,n-1..0] ])

n `c` r = div (fact n) (fact r * fact (n-r))
	where
	fact x = product [1..x]



-- Problem 55 (Haskell)

pb55 = length ( filter isLych [1..10000] )
	where
	isLych = not . any isPalin . take 50 . tail . iterate step
		where
		isPalin x = show x == reverse (show x)
		step x = x + reverseNum x
			where
			reverseNum x = read (reverse (show x)) :: Integer



-- Problem 56 (Haskell)

pb56 = maximum [ sumDigs (a^b) | a <- [1..99], b <- [1..99] ]
	where
	sumDigs :: Integer -> Int
	sumDigs x = foldr (+) 0 (getDigs x)

	getDigs :: Integer -> [Int]
	getDigs x = map digitToInt (show x)



-- Problem 63 (Haskell)

pb63 = length [ x^y | x <- [1..9], y <- [1..30], length (show (x^y)) == y ]



-- Problem 97 (Haskell)

pb97 = drop (length m - 10) m
	where
	m = show (28433*2^(7830457)+1)



-- Problem 112 (Haskell)

pb112 = take 1 ( filter (\x -> 0.99 == propBouncy x) [1586999..] )
	where
	propBouncy x = fromIntegral (noBouncy x) / fromIntegral x
	noBouncy x = length ( filter bouncy [1..x] )
	bouncy x = not ( increasing x || decreasing x )
		where
		increasing x = and ( zipWith (<=) x' (tail x') )
		decreasing x = and ( zipWith (>=) x' (tail x') )
		x' = show x
