// Problem 18 (Java)

public class euler018
{
	public static void main(String[] args)
	{
		System.out.println("---------------------------------------------------------------");
		
		int[][] arr = {
			{75, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00},
			{95, 64, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00},
			{17, 47, 82, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00},
			{18, 35, 87, 10, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00},
			{20,  4, 82, 47, 65, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00},
			{19,  1, 23, 75,  3, 34, 00, 00, 00, 00, 00, 00, 00, 00, 00},
			{88,  2, 77, 73,  7, 63, 67, 00, 00, 00, 00, 00, 00, 00, 00},
			{99, 65,  4, 28,  6, 16, 70, 92, 00, 00, 00, 00, 00, 00, 00},
			{41, 41, 26, 56, 83, 40, 80, 70, 33, 00, 00, 00, 00, 00, 00},
			{41, 48, 72, 33, 47, 32, 37, 16, 94, 29, 00, 00, 00, 00, 00},
			{53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14, 00, 00, 00, 00},
			{70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57, 00, 00, 00},
			{91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48, 00, 00},
			{63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31, 00},
			{ 4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23}
			};

		System.out.println("The original triangle is:\n");
		
		for (int i=0; i<arr.length; i++)
		{
			for (int j=0; j<arr[i].length; j++)
			{
				if (arr[i][j] != 0)
				{
					if (arr[i][j] < 10)
					{
						System.out.print(" " + arr[i][j] + " ");
					}
					else
					{
						System.out.print(arr[i][j] + " ");
					}
				}
			}
			System.out.println();
		}
		System.out.println("---------------------------------------------------------------");
		
		for (int i=1; i<arr.length; i++)
		{
			for (int j=0; j<arr[i].length; j++)
			{
				if (j == 0)
				{
					arr[i][j] += arr[i-1][j];
				}
				else if (j == arr[i].length - 1)
				{
					arr[i][j] += arr[i-1][j-1];
				}
				else
				{
					arr[i][j] += Math.max(arr[i-1][j], arr[i-1][j-1]);
				}
			}
		}
		
		System.out.println("After applying Dijkstra's algorithm, we get:\n");
		
		for (int i=0; i<arr.length; i++)
		{
			for (int j=0; j<arr[i].length; j++)
			{
				if (arr[i][j] != 0)
				{
					System.out.print(arr[i][j] + " ");
				}
			}
			System.out.println();
		}
		System.out.println("---------------------------------------------------------------");
		
		int max = Integer.MIN_VALUE;
		
		for (int r=0; r<arr[14].length; r++)
		{
			if (arr[14][r] > max) { max = arr[14][r]; }
		}
		
		System.out.println("The maximum sum in the triangle is: " + max);
	}
}
